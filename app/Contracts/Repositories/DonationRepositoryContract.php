<?php

namespace WGTS\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DonationRepository.
 *
 * @package namespace WGTS\Contracts\Repositories;
 */
interface DonationRepositoryContract extends RepositoryInterface
{
    //
}
