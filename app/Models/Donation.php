<?php

namespace WGTS\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Donation.
 *
 * @package namespace WGTS\Models;
 */
class Donation extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'transaction_id',
      'occurrence',
      'amount',
      'total',
      'campaign_id',
      'level_id',
      'form_id',
      'processor_id',
      'ref_number',
      'tender_type',
      'first',
      'last',
      'street',
      'city',
      'state',
      'zip',
      'country',
      'latitude',
      'longitude',
      'coords',
      'comments',
      'payment_made_at',

    ];

    protected $dates = ['payment_made_at'];

}
