<?php

namespace WGTS\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = ['session_id', 'last_used_at'];
    protected $dates = ['last_used_at'];
}
