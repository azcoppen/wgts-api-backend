<?php

namespace WGTS\Models;

use Illuminate\Database\Eloquent\Model;

class Check extends Model
{
    protected $dates = ['started_at', 'finished_at'];
    protected $fillable = ['started_at', 'finished_at', 'changes'];
}
