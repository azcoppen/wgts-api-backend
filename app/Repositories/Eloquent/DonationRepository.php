<?php

namespace WGTS\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use WGTS\Contracts\Repositories\DonationRepositoryContract;
use WGTS\Models\Donation;
use WGTS\Validators\DonationValidator;

/**
 * Class DonationRepositoryEloquent.
 *
 * @package namespace WGTS\Repositories\Eloquent;
 */
class DonationRepository extends BaseRepository implements DonationRepositoryContract
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Donation::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
