<?php

namespace WGTS\Http\Controllers;

use Illuminate\Http\Request;

use \Exception;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index ( Request $request )
    {
      try {
        return response()->json(['msg'=>'Hello, World']);
      } catch ( Exception $e ) {
        return $this->json_error_response ($e);
      }
    }

}
