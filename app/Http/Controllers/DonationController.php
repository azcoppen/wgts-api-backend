<?php

namespace WGTS\Http\Controllers;

use Illuminate\Http\Request;
use WGTS\Repositories\Eloquent\DonationRepository AS ModelRepository;
use WGTS\Models\Donation;
use WGTS\Transformers\DonationTransformer;
use WGTS\Criteria\DonationFilterCriteria;

use \Exception;

class DonationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index ( Request $request )
    {
        try {

          return fractal (
            app (ModelRepository::class)->pushCriteria (new DonationFilterCriteria ($request))->paginate (25),
            new DonationTransformer
          );

        } catch ( Exception $e ) {
          return $this->json_error_response ($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \WGTS\Models\Donation  $donation
     * @return \Illuminate\Http\Response
     */
    public function show ( Request $request, Donation $donation )
    {
      try {

      } catch ( Exception $e ) {
        return $this->json_error_response ($e);
      }
    }
}
