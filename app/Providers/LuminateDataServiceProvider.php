<?php

namespace WGTS\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Support\DeferrableProvider;

use WGTS\Contracts\Luminate\SessionManagementContract;
use WGTS\Contracts\Luminate\SOAPQueryContract;
use WGTS\Contracts\Luminate\DataImportContract;
use WGTS\Contracts\Luminate\DataUpsertContract;

use WGTS\Services\Luminate\DataSync\Session;
use WGTS\Services\Luminate\DataSync\SOAPQuery;
use WGTS\Services\Luminate\DataSync\DataImport;
use WGTS\Services\Luminate\DataSync\DataUpsert;

class LuminateDataServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->bind (SessionManagementContract::class, function() {
          return new Session;
      });

      $this->app->bind (SOAPQueryContract::class, function() {
          return new \SoapClient (null,
            [
              'location'            => config ('luminate.soap_url'),
              'uri'                 => 'http://schemas.xmlsoap.org/soap/envelope/',
          		'style'               => SOAP_RPC,
          		'use'                 => SOAP_ENCODED,
          		'soap_version'        => SOAP_1_1,
          		'cache_wsdl'          => WSDL_CACHE_NONE,
          		'connection_timeout'  => 15,
          		'trace'               => true,
          		'encoding'            =>'UTF-8',
          		'exceptions'          => true,
            ]
          );
      });

      $this->app->bind (DataUpsertContract::class, function() {
          return new DataUpsert;
      });

      $this->app->bind (DataImportContract::class, function() {
          return new DataImport;
      });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [SessionManagementContract::class, SOAPQueryContract::class, DataUpsertContract::class, DataImportContract::class];
    }
}
