<?php

namespace WGTS\Providers;

use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

use WGTS\Events\CheckStarted;
use WGTS\Events\CheckFinished;
use WGTS\Events\DonationTotalsChanged;

use WGTS\Listeners\CheckStartListener;
use WGTS\Listeners\CheckFinishListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        CheckStarted::class => [
          CheckStartListener::class,
        ],

        CheckFinished::class => [
          CheckFinishListener::class,
        ],

        DonationTotalsChanged::class => [

        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
