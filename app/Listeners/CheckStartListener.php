<?php

namespace WGTS\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use WGTS\Events\CheckStarted;

use WGTS\Models\Check;

class CheckStartListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckStarted  $event
     * @return void
     */
    public function handle(CheckStarted $event)
    {

    }
}
