<?php

namespace WGTS\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use WGTS\Events\CheckFinished;

class CheckFinishListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CheckFinished  $event
     * @return void
     */
    public function handle(CheckFinished $event)
    {
        //
    }
}
