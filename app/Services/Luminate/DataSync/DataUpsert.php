<?php

namespace WGTS\Services\Luminate\DataSync;

use WGTS\Contracts\Geo\GeocodingContract;
use WGTS\Contracts\Luminate\DataUpsertContract;
use Illuminate\Support\Collection;
use \Carbon\Carbon;
use \Exception;
use \Geocoder;

use WGTS\Repositories\Eloquent\DonationRepository AS ModelRepository;

class DataUpsert implements DataUpsertContract {

  private $geocoder;
  private $payload;
  private $new_records = [];

  public function __construct () {
    //
  }

  public function payload ( Collection $payload ) : self {
    $this->payload = $payload;
    return $this;
  }

  public function process () : Collection {
    if ( !$this->payload ) {
      return false;
    }

    if ( $this->payload->count () > 0 ) {

      foreach ( $this->payload AS $index => $soap_record ) {

        if ( is_object ($soap_record) ) {

          if ( !isset($soap_record->Payment) || !is_object ($soap_record->Payment) ) {
            continue;
          }

          if ( !isset($soap_record->Payment->BillingName) || !is_object ($soap_record->Payment->BillingName) ) {
            continue;
          }

          if ( !isset($soap_record->Payment->BillingAddress) || !is_object ($soap_record->Payment->BillingAddress) ) {
            continue;
          }

          if ( isset ($soap_record->TransactionId) && !empty ($soap_record->TransactionId) ) {

            if ( app (ModelRepository::class)->findByField ('transaction_id', $soap_record->TransactionId)->count() < 1 ) {

              if ( config ('luminate.geocode_addresses') ) {
                $this->geocoder = Geocoder::getCoordinatesForAddress (collect ([
                  $soap_record->Payment->BillingAddress->Street1 ?? '',
                  $soap_record->Payment->BillingAddress->City ?? '',
                  $soap_record->Payment->BillingAddress->State ?? '',
                  $soap_record->Payment->BillingAddress->Zip ?? '',
                  $soap_record->Payment->BillingAddress->Country ?? '',
                ])->implode (", "));
              } else {
                $this->geocoder = [
                  'lat' => 0,
                  'lng' => 0,
                ];
              }

              $this->new_records[] = app (ModelRepository::class)->create ([
                'transaction_id'    => $soap_record->TransactionId ?? NULL,
                'occurrence'        => isset ($soap_record->RecurringPayment) && is_object ($soap_record->RecurringPayment) ? intval($soap_record->RecurringPayment->NumberOfPayments) : 1,
    						'amount'            => floatval($soap_record->Payment->Amount) ?? NULL,
                'total'             => isset ($soap_record->RecurringPayment) && is_object ($soap_record->RecurringPayment) ? 12 * floatval($soap_record->Payment->Amount) : floatval($soap_record->Payment->Amount),
    						'campaign_id'       => $soap_record->CampaignId ?? NULL,
    						'level_id'          => $soap_record->DonationLevelId ?? NULL,
    						'form_id'           => $soap_record->FormId ?? NULL,
    						'processor_id'      => $soap_record->Payment->ProcessorTransactionId ?? NULL,
    						'ref_number'        => $soap_record->Payment->ProcessorRefNumber ?? NULL,
    						'tender_type'       => $soap_record->Payment->TenderType ?? NULL,
                'anon'              => isset ($soap_record->Recognition) && is_object ($soap_record->Recognition) ? intval ($soap_record->Recognition->IsAnonymous) : 0,
    						'first'             => $soap_record->Payment->BillingName->FirstName ?? NULL,
    						'last'              => $soap_record->Payment->BillingName->LastName ?? NULL,
    						'street'            => $soap_record->Payment->BillingAddress->Street1 ?? NULL,
    						'city'              => $soap_record->Payment->BillingAddress->City ?? NULL,
    						'state'             => $soap_record->Payment->BillingAddress->State ?? NULL,
    						'zip'               => $soap_record->Payment->BillingAddress->Zip ?? NULL,
    						'country'           => $soap_record->Payment->BillingAddress->Country ?? NULL,
    						'latitude'          => $this->geocoder['lat'] ?? 0,
    						'longitude'         => $this->geocoder['lng'] ?? 0,
    						'coords'            => \DB::raw ("ST_GeomFromText('POINT(".$this->geocoder['lng']." ".$this->geocoder['lat'].")', 4326)"),
    						'comments'          => $soap_record->Comments ?? NULL,
    						'payment_made_at'   => Carbon::parse($soap_record->Payment->PaymentDate) ?? NULL,
                'created_at'        => Carbon::parse($soap_record->Payment->CreationDate) ?? Carbon::now(),
                'updated_at'        => Carbon::parse($soap_record->Payment->ModifyDate) ?? Carbon::now(),
              ]);

            } // exists
          } // trans id
        } // is_object
      } // foreach
    } // count

    return collect ($this->new_records);

  }

}
