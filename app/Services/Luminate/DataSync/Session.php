<?php

namespace WGTS\Services\Luminate\DataSync;

use WGTS\Contracts\Luminate\SessionManagementContract;
use WGTS\Models\Session AS SessionModel;
use \Carbon\Carbon;
use \SoapFault;
use \SoapVar;
use \Exception;
use \Log;
use WGTS\Contracts\Luminate\SOAPQueryContract;
use WGTS\Exceptions\MalformedSessionException;

class Session implements SessionManagementContract {

  private $remote_session_id = null;
  private $latest_record = null;
  private $soap_client = null;

  public function __construct () {
    //
  }

  public function id () : string {

    // If the class isn't a singleton/already instantiated
    if ( !$this->remote_session_id ) {
      $this->establish ();
    }

    return $this->remote_session_id ?? '';

  }

  public function establish () : self {

    /*******************************************************************
    1. First, try the cache, as it has an auto-timeout.
    *******************************************************************/
    if ( cache ()->has ('remote_session_id') ) {
      $this->remote_session_id = cache ()->get ('remote_session_id');
      return $this;
    }

    /*******************************************************************
    // 2. If not, second, try and see if we have a recent record.
    *******************************************************************/
    $this->latest_record = SessionModel::where ('last_used_at', '>', Carbon::now()->subMinutes(config ('luminate.session_timeout')))->first();

    if ( $this->latest_record ) {
      $this->remote_session_id = $this->latest_record->session_id;
      return $this;
    }

    $this->soap_client = app (SOAPQueryContract::class);

    /*******************************************************************
    // 3. Otherwise, connect and create a new session for storing into the DB and cache.
    *******************************************************************/
    try {

      /*******************************************************************
      // We send a basic login request to the API to get a session ID..

      The query looks like this:

      /*
      <soap:Body>
        <Login xmlns='urn:soap.convio.com'>
          <UserName>data_viewer1</UserName>
          <Password>dtw1</Password>
        </Login>
      </soap:Body>

      ********************************************************************/
      $soap_response = $this->soap_client->Login (
        new SoapVar (config ('luminate.api_user'), XSD_STRING, null, null, 'UserName'),
        new SoapVar (config ('luminate.api_pass'), XSD_STRING, null, null, 'Password')
      );

      if ( $soap_response && is_object ($soap_response) && isset ($soap_response->SessionId) ) {
        $this->remote_session_id = $soap_response->SessionId;

        $this->latest_record = SessionModel::create ([
          'session_id'    => $this->remote_session_id,
          'last_used_at'  => new Carbon,
        ]);

        cache ()->put ('remote_session_id', $this->remote_session_id, config ('luminate.session_timeout'));

        return $this;

      } else {
        throw new MalformedSessionException ("SOAP response does not contain an actionable session ID.");
      }

    }
    catch ( SoapFault $e ) { // If the API returns an error, a SoapFault will be a fatal crash. exceptions = true amd trace = true needs to be set in the SoapClient constructor.
      if ( config ('luminate.log_faults') ) {
        Log::error($e);
        Log::error ($this->soap_client->__getLastRequest());
        Log::error ($this->soap_client->__getLastResponse());
      }
    }
    catch ( Exception $e ) {
      Log::error($e);
    }

    return $this;

  }

}
