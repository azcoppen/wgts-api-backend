<?php

namespace WGTS\Services\Luminate\DataSync;

use WGTS\Contracts\Luminate\SessionManagementContract;
use WGTS\Contracts\Luminate\DataImportContract;
use WGTS\Contracts\Luminate\DataUpsertContract;
use WGTS\Contracts\Luminate\SOAPQueryContract;
use Illuminate\Support\Collection;
use \Carbon\Carbon;
use \SoapHeader;
use \SoapVar;
use \SoapFault;
use \Exception;
use \Log;
use WGTS\Models\Session AS SessionModel;

class DataImport implements DataImportContract {

  private $session;
  private $additions = false;
  private $items;

  public function __construct () {
    //
  }

  public function additions () : bool {
    return $this->additions;
  }

  public function items () : Collection {
    if ($this->items)  {
      return $this->items;
    }
    return collect ([]);
  }

  public function execute () : self {

    $this->soap_query = app (SOAPQueryContract::class);

    try {

      $this->session = app (SessionManagementContract::class);

      /*******************************************************************
      // SOAP queries have to be authenticated with a session key derived
      from a previous login. Here we set the SOAP header to provide it.

      It ends up looking like this:

      <soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
      <soap:Header>
        <Session xmlns='urn:soap.convio.com'>
          <SessionId>8975a6686d01bb97b2b4a37450e0b2711dc4cfbb:JSESSIONID=49332EAF10524BF107DFDC3DAB099E3E.app30040a:1139772:4058:2019-11-27T20:12:22.234Z</SessionId>
        </Session>
      </soap:Header>
      *******************************************************************/
      $header = new SOAPHeader (config('luminate.soap_namespace'), 'Session', new SoapVar ([
          new SoapVar ($this->session->id (), XSD_STRING, null, null, 'SessionId'),
      ], SOAP_ENC_OBJECT));

      /*******************************************************************
      // Next, we send the SQL-style query to get the last 100 items since
      X mins ago. It could be shorter, but we want to catch anything queued.

      The query looks like this:

      <soap:Body>
        <Query xmlns='urn:soap.convio.com'>
          <QueryString>Select TransactionId, CampaignId, DonationLevelId, FormId, Comments, Payment.*, RecurringPayment.*  from Donation</QueryString>
          <Page>1</Page>
          <PageSize>1000</PageSize>
        </Query>
      </soap:Body>
      *******************************************************************/
      $this->soap_query->__setSoapHeaders($header);

      $soap_response = $this->soap_query->Query (
        new SoapVar ("Select TransactionId, CampaignId, DonationLevelId, FormId, Comments, Payment.*, Recognition.*, RecurringPayment.* from Donation where Payment.PaymentDate > ".env('LUMINATE_FORCE_START', Carbon::now()->subDays(1)->format('c')), XSD_STRING, null, null, 'QueryString'),
        new SoapVar (1, XSD_INT, null, null, 'Page'),
        new SoapVar (config('luminate.max_records'), XSD_STRING, null, null, 'PageSize')
      );

      /*******************************************************************
      // If we have data, we have it over to the Upserter to process.
      *******************************************************************/
      if ( $soap_response && is_array ($soap_response) && array_key_exists ('Record', $soap_response) && is_array ($soap_response['Record']) && count ($soap_response['Record']) ) {

        $this->items = app (DataUpsertContract::class)->payload (collect ($soap_response['Record']))->process ();

        // We set an extra property here to minimize the chances of any dependent code
        // not getting a Collection, as it expects.
        if ( is_object ($this->items) && is_countable ($this->items) && $this->items->count () > 0 ) {
          $this->additions = 1;
        }

      } else {
        // The response is malformed, or there are no items returned by the query. Do nothing.
      }

      /*******************************************************************
      // Update the notes for the session key.
      *******************************************************************/
      SessionModel::where ('session_id', $this->session->id ())->update ([
        'last_used_at'  => new Carbon,
      ]);

      cache ()->put ('remote_session_id', $this->session->id (), config ('luminate.session_timeout'));

    }
    catch ( SoapFault $e ) { // If the API returns an error, a SoapFault will be a fatal crash. exceptions = true amd trace = true needs to be set in the SoapClient constructor.
      if ( config ('luminate.log_faults') ) {
        Log::error($e);
        Log::error ($this->soap_query->__getLastRequest());
        Log::error ($this->soap_query->__getLastResponse());
      }
    }
    catch ( Exception $e ) {
      Log::error($e);
    }

    return $this;
  }

}
