<?php

namespace WGTS\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class DonationFilterCriteria.
 *
 * @package namespace WGTS\Criteria;
 */
class DonationFilterCriteria implements CriteriaInterface
{
    private $request;

    public function __construct ( Request $request ) {
      $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orderBy('payment_made_at', 'DESC');
    }
}
