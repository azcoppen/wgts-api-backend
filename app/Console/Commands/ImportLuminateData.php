<?php

namespace WGTS\Console\Commands;

use Illuminate\Console\Command;

use WGTS\Contracts\Luminate\DataImportContract;
use WGTS\Events\CheckStarted;
use WGTS\Events\CheckFinished;
use WGTS\Events\DonationTotalsChanged;
use \Carbon\Carbon;
use \Exception;
use WGTS\Models\Check;

class ImportLuminateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'luminate:import {--start=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Connects to the Luminate SOAP API and retrieves the most recent donations';

    private $importer;
    private $check_record;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->importer = app (DataImportContract::class);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      try {

        $this->check_record = Check::create ([
          'started_at' => new Carbon,
        ]);

        event ( new CheckStarted );

        if ( $this->importer->execute ()->additions () ) {
          event ( new DonationTotalsChanged ( $this->importer->items () ) );
        }

        $this->check_record->update ([
          'finished_at' => new Carbon,
          'changes'     => count ($this->importer->items ()) ?? 0,
        ]);

        event ( new CheckFinished ($this->importer->items ()) );

      } catch ( Exceptiom $e ) {
        \Log::error ($e);
      }
    }
}
