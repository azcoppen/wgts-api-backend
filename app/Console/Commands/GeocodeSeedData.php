<?php

namespace WGTS\Console\Commands;

use Illuminate\Console\Command;
use WGTS\Models\Donation;

class GeocodeSeedData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'donations:geocode';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Geocodes the latitude/longitude of donation records from their address';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach ( Donation::all () AS $donation ) {
          if ( $donation->lat == 0 ) {

            try {

              $address_str = collect ([
                $donation->street ?? '',
                $donation->city ?? '',
                $donation->state ?? '',
                $donation->zip ?? '',
                $donation->country ?? '',
              ])->implode (", ");

              $this->info ($address_str);

              $geocoder = \Geocoder::getCoordinatesForAddress ($address_str);

              $donation->update ([
                'latitude'  => $geocoder['lat'],
                'longitude' => $geocoder['lng'],
                'coords'    => \DB::raw ("ST_GeomFromText('POINT(".$geocoder['lng']." ".$geocoder['lat'].")', 4326)"),
              ]);

              $this->line("\t-->"."POINT(".$geocoder['lng']." ".$geocoder['lat'].")");

            } catch ( \Exception $e ) {
              $this->line ($e->getMessage());
            }

          }
        }
    }
}
