<?php

namespace WGTS\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ImportLuminateData::class,
        Commands\GeocodeSeedData::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

      switch (config ('luminate.poll_mins') ) {
        case 1:
          $schedule->command ('luminate:import')->everyMinute();
        break;

        case 5:
          $schedule->command ('luminate:import')->everyFiveMinutes();
        break;

        case 10:
          $schedule->command ('luminate:import')->everyTenMinutes();
        break;

        default:
          $schedule->command ('luminate:import')->everyMinute();
        break;
      }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
