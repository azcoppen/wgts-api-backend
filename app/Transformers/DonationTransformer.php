<?php

namespace WGTS\Transformers;

use League\Fractal\TransformerAbstract;
use WGTS\Models\Donation;

class DonationTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform ( Donation $donation )
    {
        return [
            'id'              => intval ($donation->id),
            'transaction_id'  => intval ($donation->transaction_id),
            'amount'          => floatval ($donation->amount),
            'campaign_id'     => intval ($donation->campaign_id),
            'level_id'        => intval ($donation->level_id),
            'form_id'         => intval ($donation->form_id),
            'processor_id'    => $donation->processor_id,
            'ref_number'      => $donation->ref_number,
            'tender_type'     => $donation->tender_type,
            'first'           => title_case ($donation->first),
            'last'            => title_case ($donation->last),
            'street'          => title_case ($donation->street),
            'city'            => title_case ($donation->city),
            'state'           => $donation->state,
            'zip'             => intval ($donation->zip),
            'country'         => $donation->country,
            'location'        => [
              'type' => 'Feature',
              'geometry' => [
                'type' => 'Point',
                'coordinates' => [floatval ($donation->longitude), floatval ($donation->latitude)]
              ],
              'properties' => [],
            ],
            'comments'        => $donation->comments,
            'payment_made_at' => $donation->payment_made_at->format ('c'),
            'created_at'      => $donation->created_at->format ('c'),
            'updated_at'      => $donation->updated_at->format ('c'),
        ];
    }
}
