# WGTS Luminate CRM Mirror Service: Laravel 6

Author: Alex Cameron <alex.cameron@cstraight.com>

This is a basic app to talk to the Luminate SOAP API, and store/mirror the data within it. It provides a simplistic RESTful JSON API for a client application to communicate with.

## tl;dr How Do I Run This Thing?
Just install it as normal, then set up a cron job for the scheduler. Done. The app will run from the command line every minute.

Then, check the RAML file for the API.

### Ontology
Blackbaud bought a Texas company called Convio (brand "Convio Open Platform"), which has a CRM/CMS component named "Luminate Online" and a  core contact management system named "Constituent360". They call it a "constituent engagement platform": http://www.convio.com/files/Convio-Luminate-Brochure.pdf .

### Terminology

- A user/donor or CRM entrant is a "constituent". They are organised into "groups".
- A donation is a "transaction".
- A subscription is a "recurring gift".

### API Details
The Luminate CRM/CMS platform is built on Apache Tomcat (Java servlets) and has 2 APIs: the first uses SOAP, and the other uses REST. They are jumbled in together without much structure and although they have a huge volume of documentation, it's not well structured or particularly coherent. To make it more confusing, access to the API can be from a web page (the "client" version) or from another server or app (the "server" version), both of which supply different types of data.

- Documentation: http://open.convio.com/api/#main
- The WSDL file is here: https://webservices.cluster3.convio.net/1.0/wgts/wsdl
- A SOAP test console is available here: https://webservices.cluster3.convio.net/1.0/wgts/console
- A SOAP log (time-limited) is available here: https://webservices.cluster3.convio.net/1.0/wgts/monitor

This isn't your usual API. The SOAP service needs a "login" to create a `session` and uses a SQL-style language to "query" data sources, like so:

```
Select * from Donation
```

You can find an exported request set for Postman in `resources/docs/api/postman` which illustrate the flow. Remember to replace the session ID with the one you got from the initial login request.

## Setup

Usual stuff. Set up Nginx. Create database `wgts`.

```
composer -vvv install
php artisan migrate
```

If you're using Postgres, **remember to enable Postgis** on the public schema of the DB:

```
CREATE EXTENSION postgis;
```

There's example data in the `database\dumps\` subfolder.

If you're using Nginx, remember to set a CORS policy, like so: https://enable-cors.org/server_nginx.html.

```
add_header 'Access-Control-Allow-Origin' '*';
add_header 'Access-Control-Allow-Methods' 'GET, HEAD, OPTIONS';
add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
```


You should also ban anything other than `GET`, `HEAD` and `OPTIONS`. There's no reason to be anything else than **read only**.

```
add_header Allow "GET, OPTIONS, HEAD" always;
if ( $request_method !~ ^(GET|OPTIONS|HEAD)$ ) {
	return 405;
}
```

The `routes/web.php` part is disabled as there's no need for a session or other middleware.

## Methodology

The app works primarily on the command line to query Luminate CRM every minute. It looks for a previous session ID it can use first - stored in the `sessions` table, and tries it. If it's too old, it'll log in again and store a new one. Each time it makes a request, it updates the `last_used_at` field. Timeouts are 30mins of inactivity.

The SOAP request will send a SQL-style query to retrieve all the donations in the last minute to the Convio SOAP endpoint defined in the admin panel (https://webservices.cluster3.convio.net/1.0/wgts) - meaning it's always 1 minute behind. Generally speaking, it looks like this:

```
<?xml version='1.0' encoding='UTF-8' ?>
<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
  <soap:Header>
    <Session xmlns='urn:soap.convio.com'>
      <SessionId>3d23a50b603e828cd1fb84e4a3890da31f9f862a:JSESSIONID=4373E6264B2B1B6F90CE217F7012C072.app334b:1139772:4058:2019-11-21T20:52:23.571Z</SessionId>
    </Session>
  </soap:Header>
  <soap:Body>
    <Query xmlns='urn:soap.convio.com'>
      <QueryString>select Donation.*, Payment.* from Donation where Payment.CreationDate > 2019-10-21T01:10:16Z</QueryString>
      <Page>1</Page>
      <PageSize>1000</PageSize>
    </Query>
  </soap:Body>
</soap:Envelope>
```

The app compares this list to what's in the database and _upserts_ any new entries which don't already exist.

If it finds new entries, it triggers a `DonationTotalsChanged` event which broadcasts to **Pusher** for the JS client to subscribe to and pick up - so it knows when to manually request new updated data. The idea is the HTML front end shouldn't need to live near the backend.

A `Donation` object formatted in Convio XML generally looks like this:

```xml
<Record xsi:type="ens:Donation">
	<ens:DonationLevelId>2021</ens:DonationLevelId>
	<ens:Payment>
		<ens:Amount>5.00</ens:Amount>
		<ens:PaymentDate>2019-08-19T14:52:44Z</ens:PaymentDate>
		<ens:CreationDate>2019-08-19T14:52:47Z</ens:CreationDate>
		<ens:ModifyDate>2019-08-19T14:52:47Z</ens:ModifyDate>
		<ens:BillingName>
			<ens:Title xsi:nil="true" />
			<ens:FirstName>John</ens:FirstName>
			<ens:MiddleName xsi:nil="true" />
			<ens:LastName>Doe</ens:LastName>
			<ens:Suffix xsi:nil="true" />
		</ens:BillingName>
		<ens:BillingAddress>
			<ens:Street1>123 Example Street</ens:Street1>
			<ens:Street2 xsi:nil="true" />
			<ens:Street3 xsi:nil="true" />
			<ens:County xsi:nil="true" />
			<ens:City>Some City</ens:City>
			<ens:State>DC</ens:State>
			<ens:Zip>12345</ens:Zip>
			<ens:Country>United States</ens:Country>
		</ens:BillingAddress>
		<ens:ConfirmationCode>165725</ens:ConfirmationCode>
		<ens:ConvioConfirmationCode>4058-1281-1-5702-5902</ens:ConvioConfirmationCode>
		<ens:ProcessorTransactionId>9c4ebba1-f998-4d7d-9369-92a4c899ffb7</ens:ProcessorTransactionId>
		<ens:ProcessorRefNumber>ch_1F9CDxFdj161vN8bra7Iw1Zf</ens:ProcessorRefNumber>
		<ens:TenderType>CREDIT_CARD</ens:TenderType>
		<ens:CreditCardType>VISA</ens:CreditCardType>
		<ens:CreditCardNumber>0000********0000</ens:CreditCardNumber>
		<ens:CreditCardExpDate>7/2023</ens:CreditCardExpDate>
		<ens:BatchSettlementNumber>2019-08-27-102</ens:BatchSettlementNumber>
		<ens:SettlementDate>2019-08-27T05:00:00Z</ens:SettlementDate>
		<ens:MerchantAccountId>102</ens:MerchantAccountId>
		<ens:RoutingNumber> </ens:RoutingNumber>
		<ens:TransitNumber> </ens:TransitNumber>
		<ens:BankNumber> </ens:BankNumber>
		<ens:AccountType>CHECKING</ens:AccountType>
		<ens:AccountNumber> </ens:AccountNumber>
	</ens:Payment>
</Record>

```

## IP Whitelisting
The server this runs on MUST have its IP whitelisted in the Luminate CRM admin panel or SOAP requests will be rejected.

See: https://secure3.convio.net/wgts/admin/AdminLogin

## Debugging
Laravel Telescope is available at thisinstall.local/telescope. Not on production though.

## ENV Vars

Make sure you have the following set in your .env file:

```
LUMINATE_SITE_URL=connect.wgts919.com
LUMINATE_SOAP_URL=https://webservices.cluster3.convio.net/1.0/wgts
LUMINATE_API_USER=redacted
LUMINATE_API_PASS=redacted
LUMINATE_API_KEY=redacted
LUMINATE_API_POLL_MINS=1
LUMINATE_DATA_FROM=now
LUMINATE_TIME_BACKLOG=5
LUMINATE_LOG_FAULTS=true

GMAPS_API_KEY=whatever

PUSHER_APP_ID=redacted
PUSHER_APP_KEY=redacted
PUSHER_APP_SECRET=redacted
PUSHER_APP_CLUSTER=mt1
```

These values can be found in `config/luminate.php`.

If you can, ideally you should set these upstream in Nginx, not in the application filesystem:

```
location /donations {
   fastcgi_param   LUMINATE_API_USER something;
   fastcgi_param   LUMINATE_API_PASS whatever;
}
```

## Scheduler

The SOAP polling works by looking at the .env poll frequency setting and programmatically instructing the Laravel task scheduler accordingly. You need to set up the main cron record for it to work.

When you SSH in, run `crontab -e`, then:

```
* * * * * cd /srv/example.com/laravel-dir && /usr/bin/php artisan schedule:run >> /dev/null 2>&1
```
