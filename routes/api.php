<?php

Route::get('/', 'HomeController@index');

Route::apiResource('donations', 'DonationController')->only([
    'index', 'show'
]);
