<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDonationsTable.
 */
class CreateDonationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('donations', function(Blueprint $table) {
            $table->increments('id');
						$table->string('transaction_id')->nullable()->index();
						$table->integer('occurrence')->default(0)->index();
						$table->double('amount', 8, 2)->nullable()->index();
						$table->double('total', 8, 2)->nullable()->index();
						$table->string('campaign_id')->nullable()->index();
						$table->string('level_id')->nullable()->index();
						$table->string('form_id')->nullable()->index();
						$table->string('processor_id')->nullable()->index();
						$table->string('ref_number')->nullable()->index();
						$table->string('tender_type')->nullable()->index();
						$table->boolean('anon')->default(0)->index();
						$table->string('first')->nullable()->index();
						$table->string('last')->nullable()->index();
						$table->string('street')->nullable()->index();
						$table->string('city')->nullable()->index();
						$table->string('state')->nullable()->index();
						$table->string('zip')->nullable()->index();
						$table->string('country')->nullable()->index();
						$table->decimal('latitude', 10, 7)->nullable()->index();
						$table->decimal('longitude', 10, 7)->nullable()->index();
						$table->point('coords')->nullable();
						$table->text('comments')->nullable();
						$table->timestamp('payment_made_at')->nullable()->index();
            $table->nullableTimestamps();

						$table->spatialIndex('coords');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('donations');
	}
}
