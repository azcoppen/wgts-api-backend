<?php

/******************************************************************************
CONVIO LUMINATE CRM CONFIGURATION

Luminate CRM is the online donation management platform used by WGTS. It hasn't
been updated since Blackbaud bought Convio in 2010. It has an older SOAP API
which uses a SQL-style language to retrieve data, and a newer REST version which
has an Ajax client and server-server components.

Important: access to Luminate is IP-restricted. The server running this needs its
IP whitelisted.

Product info: http://www.convio.com/files/Convio-Luminate-Brochure.pdf
Documentation: http://open.convio.com/api/#main
Admin panel: https://secure3.convio.net/wgts/admin/AdminLogin

WSDL file: https://webservices.cluster3.convio.net/1.0/wgts/wsdl
SOAP console: https://webservices.cluster3.convio.net/1.0/wgts/console
SOAP log: https://webservices.cluster3.convio.net/1.0/wgts/monitor

******************************************************************************/

return [

  /*****************************************************************************
  All Luminate customers have a multi-tenant subdomain (custom domain) the CRM
  uses to identify them.
  ******************************************************************************/
  'api_url'   => env ('LUMINATE_SITE_URL', 'connect.wgts919.com'),

  /*****************************************************************************
  Each luminate install has a unique SOAP endpoint.
  ******************************************************************************/
  'soap_url'  => env ('LUMINATE_SOAP_URL', 'https://webservices.cluster3.convio.net/1.0/wgts'),

  /*****************************************************************************
  To use the API, you must be a part of the API administrators group in the site
  admin panel.
  ******************************************************************************/
  'api_user'  => env ('LUMINATE_API_USER', 'data_viewer1'),

  /*****************************************************************************
  For server-server comms, the login_name and login_password must be supplied in each
  request.
  ******************************************************************************/
  'api_pass'  => env ('LUMINATE_API_PASS', 'dtw1'),

  /*****************************************************************************
  You can set the API key yourself. Yes, it really is 'wgts'.
  ******************************************************************************/
  'api_key'   => env ('LUMINATE_API_KEY', 'wgts'),

  /*****************************************************************************
  How often to talk to the SOAP API.
  ******************************************************************************/
  'poll_mins' => env ('LUMINATE_API_POLL_MINS', 1),

  /*****************************************************************************
  Luminate doesn't provide a "order by" or "sort" method, so we have to pull info
  which uses a WHERE datetime > isodate format.
  ******************************************************************************/
  'data_from' => env ('LUMINATE_DATA_FROM', 'now'),

  /*****************************************************************************
  SOAP faults are useful for debugging.
  ******************************************************************************/
  'log_faults' => env ('LUMINATE_LOG_FAULTS', true),

  /*****************************************************************************
  Each function has an XML namespace which needs to be specified.
  ******************************************************************************/
  'soap_namespace' => 'urn:soap.convio.com',

  /*****************************************************************************
  SOAP sessions have an implicit lifetime.
  ******************************************************************************/
  'session_timeout' => 30,

  /*****************************************************************************
  How far back we query the API for donations.
  ******************************************************************************/
  'time_backlog'    => env ('LUMINATE_TIME_BACKLOG', 5),

  /*****************************************************************************
  How many records to retrieve each time (API max is 1000)
  ******************************************************************************/
  'max_records'    => env ('LUMINATE_MAX_RECORDS', 100),

  /*****************************************************************************
  Whether or not to geocode addresses to longitude and latitude
  ******************************************************************************/
  'geocode_addresses' => env ('LUMINATE_GEOCODE', true),

];
